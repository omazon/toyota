<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="background: lightgray; margin: 0 !important;width: 730px">
    <div>
        <div style="">
            <img src="https://2cs62dg8rtd48b9qi37u9ad3-wpengine.netdna-ssl.com/wp-content/themes/cp/images/logo-cp-sg2.png" style="float: left">
            <img src="https://1byirykbid01t5tfj3vvnkmz-wpengine.netdna-ssl.com/files/2017/11/cropped-toyota-logo-1.png" style="float: right; margin-right: 60px">
        </div>
        <p style="margin-top: 40px">
            <b>Fecha: 08-03-2017</b><br>
            <b>toyota.casapellas.com</b>
        </p>
        <div style="margin-top: 20px; font-size: 26px">
            <span>PROFORMA</span>
            <span style="color: red; margin: 0 20px">TOYOTA</span>
            <span>AGYA</span>
        </div>
    </div>
    <div style="height: 5px; background-color: red;"></div>
    <div style="text-align: center">
        <img src="https://1byirykbid01t5tfj3vvnkmz-wpengine.netdna-ssl.com/files/2018/01/fit-4.png" style="width: 400px">
    </div>
    <p style="font-size: 32px">
                ROSS BUSTAMANTE
    </p>
    <table>
        <tr>
            <td style="padding-right: 60px">
                <div>
                    <table style="margin-top: 20px">
                        <tr>
                            <td><b>Categoría: </b></td>
                            <td>SEDÁN</td>
                        </tr>
                        <tr>
                            <td><b>Color: </b></td>
                            <td>Azúl</td>
                        </tr>
                        <tr>
                            <td><b>No. de Puertas: </b></td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td><b>Motor: </b></td>
                            <td>1NR-FE-1329CC</td>
                        </tr>
                        <tr>
                            <td><b>Transmisión: </b></td>
                            <td>Automática</td>
                        </tr>
                        <tr>
                            <td><b>Paquite Eléctrico: </b></td>
                            <td>Básico</td>
                        </tr>
                        <tr>
                            <td><b>Tapicería: </b></td>
                            <td>Tela</td>
                        </tr>
                        <tr>
                            <td><b>Rines: </b></td>
                            <td>Básico</td>
                        </tr>
                        <tr>
                            <td><b>Faros antiniebla: </b></td>
                            <td>Opcionales</td>
                        </tr>
                        <tr>
                            <td><b>Bolsas de aire: </b></td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td><b>Sistema de frenos: </b></td>
                            <td>ABS</td>
                        </tr>
                        <tr>
                            <td><b>Combustible: </b></td>
                            <td>Gasolina</td>
                        </tr>
                        <tr>
                            <td><b>Tanque: </b></td>
                            <td>11 Galones</td>
                        </tr>
                        <tr>
                            <td><b>Consumo: </b></td>
                            <td>17.4 km/Litro</td>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="width: 340px">
                <div style="background-color: darkgray">
                    <div style="background-color: red; padding: 40px; text-align: center">
                        <span style="color: white"><b>DETALLE MONETARIO</b></span>
                    </div>
                    <table>
                        <tr>
                            <td><b>Plazo:</b></td>
                            <td></td>
                            <td>72 meses</td>
                        </tr>
                        <tr>
                            <td><b>Prima: </b></td>
                            <td><span style="color: red">%</span></td>
                            <td>15</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 30px"><b>Tasa: </b></td>
                            <td style="padding-bottom: 30px"><span style="color: red">%</span></td>
                            <td style="padding-bottom: 30px">9.5</td>
                        </tr>
                        <tr>
                            <td><b>Desde: </b></td>
                            <td><span style="color: lightgray">($)</span></td>
                            <td><span style="color: red">12,490</span></td>
                        </tr>
                        <tr>
                            <td><b>Prima:</b></td>
                            <td><span style="color: lightgray">($)</span></td>
                            <td>1,874</td>
                        </tr>
                        <tr>
                            <td><b>Cuota Mensual Aprox.:</b></td>
                            <td><span style="color: lightgray">($)</span></td>
                            <td><span style="color: red">196</span></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <div>
        <b>NOTAS:</b>
        <ul style="font-size: 12px">
            <li>PRECIOS INCLUYEN IVA</li>
            <li>CONSULTE A NUESTROS VENDEDORES PARA MAYORES DETALLES</li>
            <li>ESTOS PRECIOS SON INICIALES, TOTAL, PRIMA Y CUOTAS PUEDEN <br> VARIAR SEGÚN DISPONIBILIDAD</li>
        </ul>
    </div>
    <div style="text-align: center">
            <img src="https://2cs62dg8rtd48b9qi37u9ad3-wpengine.netdna-ssl.com/wp-content/themes/cp/images/logo-cp-sg2.png" style="margin: auto; display: block">
            <br><span>Casa Pellas, Managua, Nicaragua</span><br>
            <span>De la rotonda el Güegüense, 350 mts al sur</span><br>
            <span>Telf: 2255-4444</span><br>
            <span>casapellas.com</span><br>
        </div>

</body>
</html>