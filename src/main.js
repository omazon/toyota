import Vue from 'vue'
import App from './App.vue'

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
window.axios = require ('axios');

import es from 'vee-validate/dist/locale/es'
import VeeValidate,{Validator} from 'vee-validate'
Validator.localize('es',es);

Vue.use(VueFormWizard);
Vue.use(VeeValidate);

new Vue({
  el: '#app',
  render: h => h(App)
});